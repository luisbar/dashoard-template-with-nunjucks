module.exports = (app) => {
  require('./security-setter')(app);
  require('./compressor')(app);
  require('./url-globalizer')(app);
  require('./cookie-parser')(app);
  require('./json-parser')(app);
  require('./url-data-parser')(app);
  require('./public-folder-setter')(app);
  require('./logger-setter')(app);
  require('./main-routes-setter')(app);
  require('./redirector')(app);
}