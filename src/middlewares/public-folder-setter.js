const path = require('path');
const express = require('express');

//Set the public folder in order to cosume resources e.g. images, css, js, ttf, etc
module.exports = (app) => {
  app.use('', express.static(path.join(__dirname, '../assets')));
}