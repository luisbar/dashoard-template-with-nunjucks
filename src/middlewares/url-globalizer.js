//Make the current url path available as a template variable
module.exports = (app) => {
  app.use((req, res, next) => {
    res.locals.url = req.originalUrl;
    next();
  });
}