module.exports = (app) => {
  app.use((req, res, next) => {
    if (req.originalUrl === '/')
      res.redirect('/transferencias')
  });
}