const cookieParser = require('cookie-parser');

//Parse cookies
module.exports = (app) => {
  app.use(cookieParser());
}
