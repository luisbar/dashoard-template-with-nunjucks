const routes = require('../routes/');

//To define main routes
module.exports = (app) => {
  app.use('/login', routes.loginRoutes);
  app.use('/transferencias', routes.transfersRoutes);
  app.use('/metricas', routes.metricsRoutes);
}