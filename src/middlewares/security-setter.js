const helmet = require('helmet');

//Set various HTTP headers for security
module.exports = (app) => {
  app.use(helmet());
}