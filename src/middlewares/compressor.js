const compression = require('compression');

//Compress response bodies using gzip
module.exports = (app) => {
  app.use(compression());
}

