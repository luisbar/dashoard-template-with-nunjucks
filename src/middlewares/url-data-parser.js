const express = require('express');

//Parse the data sent by url with querystring library
module.exports = (app) => {
  app.use(express.urlencoded({ extended: false }));
}