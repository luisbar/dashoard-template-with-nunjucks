const express = require('express');

//Parse data to application/json
module.exports = (app) => {
  app.use(express.json());
}