const expressWinston = require('express-winston');
const logger = require('../utils/logger');
//To set the logger
module.exports = (app) => {
  app.use(
    expressWinston.logger({
      winstonInstance: logger,
      meta: false
    })
  );
}