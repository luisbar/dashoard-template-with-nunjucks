const express = require('express');
const router = express.Router();
const loginController = require('../controllers/login/login');

router.get('/', loginController.showIndex);

module.exports = router;
