const express = require('express');
const router = express.Router();
const transfersController = require('../controllers/transfers/transfers');

router.get('/', transfersController.showIndex);

module.exports = router;
