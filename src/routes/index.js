const transfersRoutes = require('./transfers');
const metricsRoutes = require('./metrics');
const loginRoutes = require('./login');

module.exports = {
  transfersRoutes,
  metricsRoutes,
  loginRoutes,
};