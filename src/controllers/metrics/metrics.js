const showIndex = (req, res) => {
  res.render('src/views/metrics/metrics.njk');
}

module.exports = {
  showIndex,
};