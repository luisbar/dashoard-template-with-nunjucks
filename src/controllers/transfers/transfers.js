const showIndex = (req, res) => {
  res.render('src/views/transfers/transfers.njk');
}

module.exports = {
  showIndex,
};