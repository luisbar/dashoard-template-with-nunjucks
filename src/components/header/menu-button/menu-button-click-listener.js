const menuButton = document.getElementById('menu-button');
const menuClassList = document.getElementsByClassName('responsive-menu-container')[0].classList;
const menuButtonClassList = menuButton.classList;

menuButton.onclick = () => {
  reponsiveMenuAnimation();
  menuButtonAnimation();
}

const reponsiveMenuAnimation = () => {
  if (menuClassList.contains('menu-opened'))
    menuClassList.remove('menu-opened')
  else
    menuClassList.add('menu-opened')
}

const menuButtonAnimation = () => {
  if (menuButtonClassList.contains('menu-button-pressed'))
    menuButtonClassList.remove('menu-button-pressed')
  else
    menuButtonClassList.add('menu-button-pressed')
}

