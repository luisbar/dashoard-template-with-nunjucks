const winston = require('winston');

const errorLogger = new winston.transports.File({
  level: 'error',
  filename: 'error.log',
  format: winston.format.json()
});

const infoLogger = new winston.transports.File({
  level: 'info',
  filename: 'info.log',
  format: winston.format.json()
});

const debugLoger = new winston.transports.Console({
  format: winston.format.simple(),
  level: 'debug'
})

const logger = winston.createLogger({
  transports: [
    errorLogger,
    infoLogger
  ]
});

//If we're not in production then log to the `console` with the format:
//`${info.level}: ${info.message} JSON.stringify({ ...rest }) `
if (process.env.NODE_ENV !== 'production')
  logger.add(debugLoger);

module.exports = logger;
