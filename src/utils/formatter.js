const moment = require("moment-timezone");

function numberToPrice(value, options = {}) {
  const showDecimals = options.showDecimals !== false;
  const showSymbol = options.showSymbol !== false;

  if (value && isNaN(value)) {
    // not a number? default to 0
    value = "0";
  }

  // format as USD for now, so it outputs `$123.456,00`
  // using `es-CO` and currency `COP` it outputs `COP123.456,00`
  const formatter = new Intl.NumberFormat("en-US", {
    currency: "USD",
    style: "currency",
    currencyDisplay: showSymbol ? "symbol" : "code",
    minimumFractionDigits: showDecimals ? 2 : 0,
    maximumFractionDigits: showDecimals ? 2 : 0
  });

  return formatter.format(value);
}

function formatPrice(numberString) {
  return numberToPrice(numberString, { showSymbol: true });
}

function formatDate(ISOString, options = {}) {
  const format = options.showTime ? "MM/DD/YYYY HH:mm:ss" : "MM/DD/YYYY";

  return moment(ISOString)
    .utc()
    .tz("America/Bogota")
    .format(format);
}

function formatType(type) {
  const values = {
    SEND: "ENVIAR",
    REQUEST: "SOLICITAR"
  };

  const result = values[type];

  return result ? result : "";
}

function formatStatus(status) {
  const values = {
    ERROR: "Fallida",
    TIMEOUT: "Fallida",
    INITIATED: "Pendiente",
    PENDING: "Pendiente",
    REJECTED: "Rechazada",
    FRAUD: "Rechazada",
    COMPLETED: "Aprobada"
  };

  const formattedStatus = values[status];

  return formattedStatus ? formattedStatus : "";
}

module.exports = {
  numberToPrice,
  formatPrice,
  formatDate,
  formatType,
  formatStatus
};
