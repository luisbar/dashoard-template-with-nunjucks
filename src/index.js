const express = require('express');
const nunjucks = require('nunjucks');

const formatter = require('./utils/formatter');

//Create express instance
const app = express();

//Initialize middlewares
require('./middlewares/')(app);

//Configure nunjucks template language
const nunjucksInstance = nunjucks.configure('.', {
  autoescape: true,
  express: app,
  noCache: app.get('env') === 'development',
});

//Set view engine to Nunjucks
app.set('view engine', 'njk');

// add all formatting functions as nunjucks filters
Object.keys(formatter).forEach(key => nunjucksInstance.addFilter(key, formatter[key]));

//Create web server instance
app.listen(process.env.PORT || '3000');
