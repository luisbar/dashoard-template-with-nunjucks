#### How to run
```sh
npm i && npm start
```

#### Stack and features

- Express
- Scss
- Nunjucks
- Bootstrap
- Winston
- Compression
- Cookie parser
- Helmet
- Moment timezone
- Dynamic import using webpack
- Clean structure
```
.
├── Readme.md
├── config
│   ├── util
│   │   └── entryLoader.js
│   ├── webpack.common.js
│   ├── webpack.dev.js
│   └── webpack.prod.js
├── error.log
├── info.log
├── package.json
├── postcss.config.js
└── src
    ├── assets
    │   ├── css
    │   ├── fonts
    │   └── js
    ├── components
    │   ├── header
    │   │   ├── header.njk
    │   │   └── menu-button
    │   │       ├── menu-button-click-listener.js
    │   │       ├── menu-button.njk
    │   │       └── menu-button.scss
    │   ├── responsive-menu
    │   │   ├── responsive-menu.njk
    │   │   └── responsive-menu.scss
    │   └── unresponsive-menu
    │       └── unresponsive-menu.njk
    ├── controllers
    │   ├── login
    │   │   └── login.js
    │   ├── metrics
    │   │   └── metrics.js
    │   └── transfers
    │       └── transfers.js
    ├── index.js
    ├── middlewares
    │   ├── compressor.js
    │   ├── cookie-parser.js
    │   ├── index.js
    │   ├── json-parser.js
    │   ├── logger-setter.js
    │   ├── main-routes-setter.js
    │   ├── public-folder-setter.js
    │   ├── redirector.js
    │   ├── security-setter.js
    │   ├── url-data-parser.js
    │   └── url-globalizer.js
    ├── routes
    │   ├── index.js
    │   ├── login.js
    │   ├── metrics.js
    │   └── transfers.js
    ├── theme
    │   ├── colors.scss
    │   ├── font.scss
    │   ├── icons.scss
    │   └── theme.scss
    ├── utils
    │   ├── formatter.js
    │   └── logger.js
    └── views
        ├── core
        │   └── default.njk
        ├── login
        │   └── login.njk
        ├── metrics
        │   ├── metrics.njk
        │   └── metrics.scss
        └── transfers
            ├── transfers.njk
            └── transfers.scss
```