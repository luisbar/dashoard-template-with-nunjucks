const path = require('path');
const entryLoader = require('./util/entryLoader');
let jsEntries = entryLoader.loadEntriesForJs();
let scssEntries = entryLoader.loadEntriesForScss();

const js = {
  entry: jsEntries,
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, '../src/assets/js'),
  },
};

const scss = {
  entry: scssEntries,
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, '../src/assets/css'),
  },
};

module.exports = [
  js,
  scss
];