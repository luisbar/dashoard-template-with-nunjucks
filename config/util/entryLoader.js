const path = require('path');
const glob = require('glob');

const loadEntriesForJs = (jsEntries = {}) => {
  const viewScripts = getFilePathsFromPath('../../src/views/**/*.js');
  const componentScripts = getFilePathsFromPath('../../src/components/**/*.js');
  const scripts = viewScripts.concat(componentScripts);

  scripts.forEach(populateEntryObject(jsEntries));

  return jsEntries;
}

const loadEntriesForScss = (scssEntries = {}) => {
  const viewStyles = getFilePathsFromPath('../../src/views/**/*.scss');
  const componentStyles = getFilePathsFromPath('../../src/components/**/*.scss');
  const styles = viewStyles
                 .concat(componentStyles)
                 .concat([path.resolve(__dirname, '../../src/theme/theme.scss')]);
  
  styles.forEach(populateEntryObject(scssEntries));

  return scssEntries;
}

const getFilePathsFromPath = (filePath) => {
  return glob.sync(path.resolve(__dirname, filePath));
}

const populateEntryObject = (entryObject) => (scriptPath) => {
  const scriptPathSplitted = scriptPath.split('/');
  const scriptPathSplittedLength = scriptPathSplitted.length;
  const scriptName = scriptPathSplitted[scriptPathSplittedLength-1].split('.')[0];

  entryObject[scriptName] = scriptPath;
}

module.exports = {
  loadEntriesForJs,
  loadEntriesForScss
};