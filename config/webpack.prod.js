const merge = require('webpack-merge');
const common = require('./webpack.common.js');

const js = merge(common[0], {
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
          }
        ]
      },
    ],
  },
});

const scss = merge(common[1], {
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].css'
            }
          },
          {
            loader: 'extract-loader'
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'postcss-loader'
          },
          {
            loader: 'sass-loader'
          },
        ],
      },
    ],
  },
});

if (Object.keys(js.entry).length && Object.keys(scss.entry).length)
  module.exports = [
    js,
    scss
  ];
else if (Object.keys(js.entry).length)
  module.exports = [
    js,
  ];
else
  module.exports = [
    scss,
  ];